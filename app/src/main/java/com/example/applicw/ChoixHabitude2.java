package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Vector;

public class ChoixHabitude2 extends AppCompatActivity {
    LinearLayout MonLayout ;
    String csvLabel;
    Vector<CheckBox> LesHabs = new Vector<>(); // Habitudes à cocher

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_habitude2);

        Button valider;
        Vector<String> MesHabitudes = new Vector<>();


        //selection des habitudes à partir du fichier csv
        InputStream inputStream;
        //Selection du fichier cmd de base ou de celui modifié par l'application
        try {
            inputStream = openFileInput("habitudes.csv");
        } catch (FileNotFoundException e) {
            inputStream = getResources().openRawResource((R.raw.habitudes));
        }

        String[] ids;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            MonLayout = findViewById(R.id.layoutHabitudes);
            csvLabel = reader.readLine(); //On passe la première ligne descriptive

            while ((csvLine = reader.readLine()) != null) {
                ids = csvLine.split(";");
                MesHabitudes.add(csvLine);
                CheckBox checkbox = new CheckBox(this);
                checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    // Pour les graphismes
                    public void onClick(View v) {
                        if (checkbox.isChecked()) {
                            checkbox.setTextColor(getResources().getColor(R.color.colorAccent));
                        }
                        else {
                            checkbox.setTextColor(getResources().getColor(R.color.black));
                        }
                    }
                    });
                    String habitudeName = ids[1];
                    checkbox.setText(habitudeName);
                    LesHabs.add(checkbox);
                    TextView description = new TextView(this);
                    description.setText(ids[3]);
                    // Si l'activité est deja en cours on ne l'affiche pas
                    if (Integer.parseInt(ids[2]) == 0) {
                        MonLayout.addView(checkbox);
                        MonLayout.addView(description);
                        Log.i("Fonctionnement",""+ "Ajout d'une habitude et de sa déccription " +ids[1]) ;
                    }
                }
        }
        catch (IOException ex) {
            Log.i("Fonctionnement"," Fichier csv illisble");
        }
        valider = findViewById(R.id.valider_button) ;
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Iterator<CheckBox> iterator = LesHabs.iterator();
                int i = 0;
                String[] ids;
                StringBuilder data = new StringBuilder(); //outils facilitant l'écriture sur le fichier csv
                data.append(csvLabel).append("\n");
                while (iterator.hasNext()) {
                    CheckBox Habitude_to_send = iterator.next();

                    if (Habitude_to_send.isChecked()) {
                        ids =MesHabitudes.get(i).split(";");
                        data.append(ids[0]+";"
                                +ids[1]+";"
                                +"1"+";"
                                +ids[3]+";"
                                +ids[4]
                                +"\n");
                    }
                    else{
                        data.append(MesHabitudes.get(i)).append("\n");

                    }
                    i++;
                }
                try {
                    // saving the file into device
                    FileOutputStream out = openFileOutput("habitudes.csv", Context.MODE_PRIVATE);
                    out.write((data.toString()).getBytes());
                    Log.i("Fonctionnement"," Fichier enregistré avec succès");
                    out.close();
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                ActiviteSuivante();
            }

        });



    }
    /**
     * S'active lors de l'appui sur le bouton 'VALIDER !'.Eenvoie vers l'activité 'ListeHabitudes'.
     *
     * @author Lucas
     */
    private void ActiviteSuivante() {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), ListeHabitudes.class);
        startActivity(messageVersActivity);
        finish();
    }


}
