package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Calendrier extends AppCompatActivity {

    ConstraintLayout calendrier;
    SharedPreferences habitude_xml;

    Boolean jour_valide = false;        //contient le fait que le jour actuel aie été selectionné
    int jour_actuel;                    //contient le numéro du jour actuel
    int nb_jours_habitude = 10;
    int duree_jour = 60*60*24;
    int duree_habitude = 60*60*24*nb_jours_habitude;
    Long nb_sec_debut;
    Long nb_sec_actuel;
    String nom_habitude;

    Button[] tab;           //tableau qui contient les view associées aux boutons
    String[] jours;         //tableau qui contient les tags des jours stockés dans nom_habitude.xml



    /**
     * Récupère la durée actuelle (relativement à une date fixe).
     * Récupère le nom de l'habitude transmis par ListeHabitudes et l'affiche en haut de page.
     * Accède au fichier nom_habitude.xml grace au nom récupéré.
     * Si l'habitude est en cours :
     *      Utilise les méthode validerJour() et couleursCases()
     * Sinon :
     *      Il faut d'abord initialiser l'habitude avec initHabitude() avant de travailler dessus en
     *      appelant validerJour(). Il n'est pas nécessaire d'utiliser couleursCases() puisque
     *      toutes les cases doivent rester grises.
     *
     * @author Matiss
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendrier);

        calendrier = findViewById(R.id.calendrier);

        tab = new Button[] {findViewById(R.id.button1), findViewById(R.id.button2),
                findViewById(R.id.button3), findViewById(R.id.button4), findViewById(R.id.button5),
                findViewById(R.id.button6), findViewById(R.id.button7), findViewById(R.id.button8),
                findViewById(R.id.button9), findViewById(R.id.button10)};

        jours = new String[] {"jour1", "jour2", "jour3", "jour4", "jour5", "jour6",
                "jour7", "jour8", "jour9", "jour10"};

        recuperationTemps();

        Intent habitude = getIntent();
        nom_habitude = habitude.getStringExtra("nom_habitude");

        affichageNomHabitude();

        habitude_xml = getSharedPreferences(nom_habitude, Context.MODE_PRIVATE);

        if (habitude_xml.getBoolean("en_cours", false)) {
            validerJour();
            couleursCases();
        }

        else {
            initHabitude();
            validerJour();
        }
    }



    /**
     * Stocke dans la variable nb_sec_actuel le nombre de secondes écoulées depuis le 01/01/1970.
     *
     * @author Matiss
     */
    private void recuperationTemps() {
        nb_sec_actuel = System.currentTimeMillis()/1000;
    }



    /**
     * Insère le nom de l'habitude (nom_habitude) dans le TextView en haut de page.
     *
     * @author Matiss
     */
    private void affichageNomHabitude() {
        TextView titre = findViewById(R.id.habitude);
        titre.setText(nom_habitude);
    }



    /**
     * Récupère le temps écoulé depuis le début de l'habitude dans la variable deltaTemps.
     * Récupère dans la variable jour_actuel le numéro du jour en cours grâce à nb_sec_actuel et
     * nb_sec_debut.
     * Si deltaTemps est strictement négatif :
     *      Il y a un problème de calendrier qui est signalé à l'utilisateur.
     * Si deltaTemps est supérieur strictement à la durée de l'habitude :
     *      On renvoie l'utilisateur vers l'activité PageBravo en lui transmettant le nom de
     *      l'habitude et le nombre de jours où l'habitude a été respectée.
     * Sinon :
     *      Seul le bouton jour_actuel devient cliquable.
     *      Si jour_valide=true, i.e. l'utilisateur a cliqué sur le jour pour le valider ensuite:
     *          Le bouton devient vert (0xFF009688) et affiche "Validez pour confirmer".
     *      Sinon :
     *          Le bouton devient gris (0xFF999999) et affiche
     *          "Cliquez si vous avez rempli la tâche".
     *
     * @author Matiss
     */
    private void validerJour() {
        nb_sec_debut = habitude_xml.getLong("debut", 0);

        Long deltaTemps = nb_sec_actuel-nb_sec_debut;
        Long nb_jours = (deltaTemps/duree_jour);
        jour_actuel = nb_jours.intValue();


        if (deltaTemps < 0) {
            Toast.makeText(this, "Erreur: vérifier la date dans les paramètres du " +
                    "téléphone...", Toast.LENGTH_LONG).show();
        }

        if (deltaTemps > duree_habitude) {
            Intent vers_resultat = new Intent();
            vers_resultat.setClass(this, PageBravo.class);
            vers_resultat.putExtra("nom_habitude", nom_habitude);

            //On calcule le nombre de jours où l'habitude a été respectée.
            int i = 0;
            for (int j=0; j<nb_jours_habitude; j++) {
                if (habitude_xml.getBoolean(jours[j], false)) {
                    i++;
                }
            }
            vers_resultat.putExtra("nb_jours", i);
            startActivity(vers_resultat);
            finish();
        }

        if ((deltaTemps >= 0) && (deltaTemps <= duree_habitude)) {
            int j = jour_actuel;
            tab[j].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!jour_valide) {
                        tab[j].setText("Validez pour confirmer");
                        jour_valide=true;
                        tab[j].setBackgroundColor(0xFF009688);
                    }
                    else {
                        tab[j].setText("Cliquez si vous avez rempli la tâche");
                        jour_valide=false;
                        tab[j].setBackgroundColor(0xFF999999);
                    }
                }
            });
        }

    }



    /**
     * Initialise le fichier nom_habitude.xml avec des valeurs par défaut, à savoir :
     *
     * {'terminee':false, 'en_cours':true, 'date_début':nb_sec_actuel,
     *      'jour1':false, ..., 'jour10':false}
     *
     * et affiche le pop-up 'Bienvenue sur votre nouvelle habitude !'.
     *
     * @author Matiss
     */
    private void initHabitude() {
        SharedPreferences.Editor edit = habitude_xml.edit();
        edit.putBoolean("terminee", false);
        edit.putBoolean("en_cours", true);
        edit.putLong("debut", nb_sec_actuel);
        edit.putBoolean("jour1", false);
        edit.putBoolean("jour2", false);
        edit.putBoolean("jour3", false);
        edit.putBoolean("jour4", false);
        edit.putBoolean("jour5", false);
        edit.putBoolean("jour6", false);
        edit.putBoolean("jour7", false);
        edit.putBoolean("jour8", false);
        edit.putBoolean("jour9", false);
        edit.putBoolean("jour10", false);
        edit.apply();
        Toast.makeText(this, "Bienvenue sur votre nouvelle habitude !", Toast.LENGTH_SHORT).show();
    }



    /**
     * S'active lors de l'appui sur le bouton 'HOME'. Renvoie vers l'activité 'MainActivity'.
     *
     * @param view : Le bouton 'HOME'
     *
     * @author Matiss
     */
    public void home(View view) {
        Intent home = new Intent();
        home.setClass(getApplicationContext(), MainActivity.class);
        startActivity(home);
        finish();
    }



    /**
     * S'active lors de l'appui sur le bouton 'HOME'.
     * Renvoie vers l'activité 'Suppression' en lui transmettant le nom de l'habitude à supprimer.
     *
     * @param view : Le bouton 'SUPPRIMER'.
     *
     * @author Matiss
     */
    public void supprimer(View view) {
        Intent suppression = new Intent();
        suppression.setClass(this, Suppression.class);
        suppression.putExtra("nom_habitude", nom_habitude);
        startActivity(suppression);
        finish();
    }



    /**
     * Associe aux boutons une couleur :
     *      -Si le jour X a été validé, le bouton est vert;
     *      -Sinon,
     *          -Si le jour est déjà passé, le bouton est rouge;
     *          -Si le jour n'est pas encore passé, il reste gris.
     * Renvoie vers l'activité 'Suppression' en lui transmettant le nom de l'habitude à supprimer.
     *
     * @author Matiss
     */
    private void couleursCases() {
        for (int j = 0; j <tab.length; j++){
            if (habitude_xml.getBoolean(jours[j], false)) {
                tab[j].setBackgroundColor(0xFF009688);
            }
            else {
                if (j<jour_actuel)
                    tab[j].setBackgroundColor(0xFFE91E63);
            }
        }
    }



    /**
     * S'active lors de l'appui sur le bouton 'VALIDER'.
     * On enregistre dans 'jourX' le booléen jour_valide déterminant si la journee a été validée.
     * On renvoie un pop-up disant que la validation a été prise en compte.
     *      Si on vient de valider le dernier jour :
     *              On renvoie vers l'activité PageBravo à laquelle on transmet le nom de l'habitude et le
     *              nombre de jours pour lesquels l'habitude a été respectée.
     *      Sinon on ferme l'activité en revenant à ListeHabitudes
     *
     * @param view : Le bouton 'VALIDER'.
     *
     * @author Matiss
     */
    public void valider(View view) {
        SharedPreferences.Editor edit = habitude_xml.edit();
        edit.putBoolean(jours[jour_actuel], jour_valide);
        edit.apply();
        Toast.makeText(this, "Enregistré !", Toast.LENGTH_SHORT).show();


        if (habitude_xml.getBoolean(jours[9], false)) {
            Intent vers_resultat = new Intent();
            vers_resultat.setClass(this, PageBravo.class);
            vers_resultat.putExtra("nom_habitude", nom_habitude);

            //On calcule le nombre de jours où l'habitude a été respectée.
            int i = 0;
            for (int j=0; j<nb_jours_habitude; j++) {
                if (habitude_xml.getBoolean(jours[j], false)) {
                    i++;
                }
            }

            vers_resultat.putExtra("nb_jours", i);
            startActivity(vers_resultat);
            finish();
        }

        else {
            Intent close = new Intent();
            close.setClass(this, ListeHabitudes.class);
            startActivity(close);
            finish();
        }
    }
}