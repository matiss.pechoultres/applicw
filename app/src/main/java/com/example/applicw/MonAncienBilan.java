package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MonAncienBilan extends AppCompatActivity {

    SharedPreferences sp;
    float bilan_carbone = (float) 6.4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_ancien_bilan);

        sp = getSharedPreferences("memBilan", Context.MODE_PRIVATE);

        if (sp.contains("monbilan")){
            bilan_carbone = sp.getFloat("monbilan", 0);
        }

        TextView tvMessage = findViewById(R.id.mon_bilan);
        int unicode = 0x1F389;
        String emoji = getEmoji(unicode);
        String message = "Votre bilan carbone actuel est de : " + bilan_carbone + " tCO2e.\n " +
                "Accomplissez vos habitudes quotidiennes ou ajoutez-en de nouvelles pour le diminuer !"+emoji;
        tvMessage.setText(message);
    }


    public String getEmoji (int uni){
        return new String(Character.toChars(uni));
    }


    /**
     * S'active lors de l'appui sur le bouton 'HOME'. Renvoie vers l'activité 'MainActivity'.
     *
     * @param view : Le bouton 'HOME'.
     *
     * @author Martin
     */
    public void home(View view) {
        Intent home = new Intent();
        home.setClass(getApplicationContext(), MainActivity.class);
        startActivity(home);
        finish();
    }
}