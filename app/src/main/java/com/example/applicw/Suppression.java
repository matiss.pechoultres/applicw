package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Suppression extends AppCompatActivity {

    SharedPreferences sp;
    String nom_habitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suppression);
        Intent suppression = getIntent();
        nom_habitude = suppression.getStringExtra("nom_habitude");
        sp = getSharedPreferences(nom_habitude, Context.MODE_PRIVATE);
    }
    /**
     * S'active lors de l'appui sur le bouton 'OUI'. Crée une pop_up, change le fichier shared preferences, le boolean en cours passe
     * à false, elle appelle la fonction changercsv. Et ramène à la page ListeHabitude.
     *
     * @param view : Le bouton 'OUI'.
     *
     * @author Matiss
     */
    public void supprimer(View view) throws IOException {
        Toast.makeText(this, "L'habitude a été supprimée", Toast.LENGTH_SHORT).show();
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean("en_cours", false);
        edit.apply();
        // On remet l'etat de l'habitude à 0 (à l'avenir on peut en mettre un autre pour ne plus la voir nulle part!
        try {
            changercsv();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Intent habitude_supprimee = new Intent();
        habitude_supprimee.setClass(this, ListeHabitudes.class);
        startActivity(habitude_supprimee);
        finish();
    }
    /**
     * S'active lors de l'appui sur le bouton 'NON'. Annule la suppression, retour à l'activity ListeHabitude
     *
     * @param view : Le bouton 'NON'.
     *
     * @author Matiss
     */
    public void retour(View view) {
        Intent habitude = new Intent();
        habitude.setClass(this, Calendrier.class);
        habitude.putExtra("nom_habitude", nom_habitude);
        startActivity(habitude);
        finish();

    }
    /**
     * S'active lors dans la méthode supprimer. On réecrit le fichier CSV en mettant l'etat de l'habitude à 0 pour pouvoir la choisir à nouveau !
     *
     * @author Lucas
     */
    private void changercsv() throws IOException {
        InputStream inputStream = openFileInput("habitudes.csv");
        String[] ids;
        String csvLine;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder data = new StringBuilder();
        while ((csvLine = reader.readLine()) != null) {
            ids = csvLine.split(";");
            String name = ids[1];
            if (name.equals(nom_habitude)){
                data.append(ids[0]+";"
                        +ids[1]+";"
                        +"0"+";"  //on change par 0
                        +ids[3]+";"
                        +ids[4]
                        +"\n");
            }
            else{
                data.append(csvLine).append("\n");
            }
        }
        inputStream.close();
        try {
            // saving the file into device
            FileOutputStream out = openFileOutput("habitudes.csv", Context.MODE_PRIVATE);
            out.write((data.toString()).getBytes());
            out.close();
            Log.i("Fonctionnement"," Fichier enregistré avec succès");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        }

    }
