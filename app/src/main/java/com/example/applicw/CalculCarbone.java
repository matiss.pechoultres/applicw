package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

public class CalculCarbone extends AppCompatActivity {

    float bilan;
    String url = "https://nosgestesclimat.fr/simulateur/bilan";



    /**
     * Lance une activité qui ouvre url dans le web browser par défaut.
     *
     * @author Caroline
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcul_carbone);

        Toast.makeText(this, "Revenez ensuite sur l'application pour rentrer votre bilan.", Toast.LENGTH_LONG).show();

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        Intent choixWebBrowser = Intent.createChooser(i, "Avec quelle app ?");
        startActivity(choixWebBrowser);
    }



    /**
     * S'active lors de l'appui sur le bouton 'CHANGER HABITUDES !'.Enregistre le bilan carbonne'.
     *
     * @author Caroline
     */
    public void changerHabitudes(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(this, ChoixHabitude2.class);

        EditText et = findViewById(R.id.editBilan);
        bilan = (float) Integer.parseInt(et.getText().toString());
        SharedPreferences sp = getSharedPreferences("memBilan", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat("monbilan", bilan);
        editor.apply();
        Log.i("Fonctionnement","Bilan carbonne enregistré");
        startActivity(messageVersActivity);
        finish();
    }
}