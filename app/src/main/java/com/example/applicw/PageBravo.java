package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PageBravo extends AppCompatActivity {

    SharedPreferences sp;
    SharedPreferences fichier_habitude;
    TextView reussite;
    TextView bilan;
    Button send_message;
    String nom_habitude;
    int nb_jours_respectes;
    int nb_jours_min_pour_valider = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_bravo);

        reussite = findViewById(R.id.text_reussite);
        bilan = findViewById(R.id.text_bilan_carbone);
        send_message = findViewById(R.id.partager);

        Intent fin_habitude = getIntent();
        nom_habitude = fin_habitude.getStringExtra("nom_habitude");
        nb_jours_respectes = fin_habitude.getIntExtra("nb_jours", 0);

        sp = getSharedPreferences("memBilan", Context.MODE_PRIVATE);
        fichier_habitude = getSharedPreferences(nom_habitude, Context.MODE_PRIVATE);


        if (nb_jours_respectes >= nb_jours_min_pour_valider) {
            reussi();
            habitudeTerminee();
        }
        else {
            rate();
            habitudeTerminee();
        }

    }


    /**
     * Modifie le fichier nom_habitude.xml pour indiquer que l'habitude :
     *      -n'est plus en cours;
     *      -a été adoptée.
     *
     * @author Caroline, Martin, Matiss
     */
    private void habitudeTerminee() {
        SharedPreferences.Editor edit = fichier_habitude.edit();
        edit.putBoolean("en_cours", false);
        edit.putBoolean("terminee", true);
        edit.apply();
    }



    /**
     * Affiche un pop-up indiquand que l'habitude a été adoptée.
     *
     * @author Caroline, Martin, Matiss
     */
    public void reussi() {
        reussite.setText("Félicitations ! Vous avez adopté une nouvelle habitude !");
    }



    /**
     * Affiche un pop-up indiquand que l'habitude n'a pas été adoptée et cache le bouton de partage.
     *
     * @author Caroline, Martin, Matiss
     */
    public void rate() {
        reussite.setText("Malheureusement, vous n'avez pas adopté l'habitude. \n \n Nous vous invitons vivement à réessayer, vous pouvez le faire !");
        send_message.setVisibility(send_message.INVISIBLE);
    }



    /**
     * Ouvre l'application SMS laissée libre de choix pour envoyer un message
     *
     * @param view : Bouton de partage
     *
     * @author Caroline, Martin, Matiss
     */
    public void envoyer(View view) {
        Intent versAppSMS = new Intent(Intent.ACTION_SENDTO);
        versAppSMS.setData(Uri.parse("smsto:"));
        versAppSMS.putExtra("sms_body","Salut ! J'ai pris la nouvelle habitude suivante: \n '" + nom_habitude + "'\n Adopte-la aussi avec Objectif2Tonnes !");
        Intent choixAppSMS = Intent.createChooser(versAppSMS, "Avec quelle app ?");
        startActivity(choixAppSMS);
    }


    /**
     * Lance l'activité MainActivity et finit celle en cours.
     *
     * @param view : Bouton 'HOME'
     *
     * @author Caroline, Martin, Matiss
     */
    public void home(View view) {
        Intent home = new Intent();
        home.setClass(getApplicationContext(), MainActivity.class);
        startActivity(home);
        finish();
    }



    /**
     * Lance l'activité ListeHabitudes et finit celle en cours.
     *
     * @param view : Bouton de retour à la liste des habitudes.
     *
     * @author Caroline, Martin, Matiss
     */
    public void listeHab(View view) {
        Intent home = new Intent();
        home.setClass(getApplicationContext(), ListeHabitudes.class);
        startActivity(home);
        finish();
    }
}

