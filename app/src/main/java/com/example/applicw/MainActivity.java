package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changerHabitudes(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), ChoixHabitude2.class);
        startActivity(messageVersActivity);
        finish();
    }

    public void changerCarbone(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), CalculCarbone.class);
        startActivity(messageVersActivity);
        finish();
    }

    public void changerMesHab(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), ListeHabitudes.class);
        startActivity(messageVersActivity);
        finish();
    }

    public void changerAncienBilan(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), MonAncienBilan.class);
        startActivity(messageVersActivity);
        finish();
    }
}