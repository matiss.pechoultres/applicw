package com.example.applicw;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ListeHabitudes extends AppCompatActivity {

    LinearLayout ll;
    String[] ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_habitudes);

        ll = findViewById(R.id.Scrollhab);
        String csvLine; //ligne du fichier csv
        InputStream inputStream;
        try {
            inputStream = openFileInput("habitudes.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            boolean empty = true; // Pas d'habitudes
            while ((csvLine = reader.readLine()) != null) {
                ids=csvLine.split(";");
                try{
                    if (Integer.parseInt(ids[2])==1) {
                        Log.i("Fonctionnement",""+"Ajout d'une habitude "+ids[1]) ;
                        empty = false; //au moins une habitude
                        //on ajoute un bouton avec l'habitude et on l'envoit à matiss
                        Button button = new Button(getApplicationContext());
                        button.setText(ids[1]);
                        ll.addView(button);
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Button b = (Button)v;
                                Intent messageVersCalendrier = new Intent();
                                String message = b.getText().toString();
                                messageVersCalendrier.setClass(getApplicationContext(), Calendrier.class);
                                messageVersCalendrier.putExtra("nom_habitude",message);
                                startActivity(messageVersCalendrier);
                                finish();
                            }
                        });
                    }
                    Log.i("Column 1 ",""+ids[2]) ;
                }catch (Exception e){
                    Log.i("Fonctionnement"," Erreur lors de l'affichage des habitudes") ;
                }
            }
            if (empty)
                listevide();
        }
        catch (IOException ex) {
            listevide();
        }

    }
    /**
     * S'active quand la liste d'activité en cours est vide. Renvoie vers l'activité 'MainActivity'.
     *
     * @author Lucas
     */
    private void listevide() {
        Toast.makeText(this, "Pas d'habitude sélectionné", Toast.LENGTH_SHORT).show();
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), MainActivity.class);
        startActivity(messageVersActivity);
        finish();
    }
    /**
     * S'active lors de l'appui sur le bouton 'RETOUR'. Renvoie vers l'activité 'MainActivity'.
     *
     * @param view : Le bouton 'RETOUR'.
     *
     * @author Martin
     */
    public void retour(View view) {
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), MainActivity.class);
        startActivity(messageVersActivity);
    }


}